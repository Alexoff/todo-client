export enum TypeAdd {
  Undefined = 'Undefined',
  AddProject = 'AddProject',
  AddTask = 'AddTask'
}
